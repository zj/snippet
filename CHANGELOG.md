# [Unreleased]

# [0.4.0]

### Added

- Add a `--host` to support hosts other than GitLab.com !11

### Changed

- Support Go 1.12 aswell as Go 1.11 !10

# [0.3.0]

### Changed

- Improve CLI interface, and help message !8
    - NOTE: This is a breaking change, execute `snippet --help` for the updated flags
- Use `go mod` for dependency management !7

# [0.2.0]

### Added

- `-file` flag to post a file instead of the piped input !4
- Support only the path as argument !5

## [0.1.0]

### Added

- Everything needed to post new snippets on GitLab.com - 3c82e9da47a5aaddc996e1242606c2b387787bb2
- Verion flag - !2

### Fixed

- On error exit immediatly with a non zero exit code - !3
- Use `dep` instead of `godep` - 79543ce58108bad38b642d25a32e3dd4b1866a32

[unreleased]: https://gitlab.com/zj/snippet/compare/0.4.0...master
[0.4.0]: https://gitlab.com/zj/snippet/compare/0.3.0...0.4.0
[0.3.0]: https://gitlab.com/zj/snippet/compare/0.2.0...0.3.0
[0.2.0]: https://gitlab.com/zj/snippet/compare/0.1.0...0.2.0
[0.1.0]: https://gitlab.com/zj/snippet/tags/0.1.0
