## Snippet

A terminal based interface to create a new GitLab snippet.

```bash
head -n 100 path/to/file | snippet
# => https://gitlab.com/snippets/<id>
```

### Installation

When running Arch Linux, a install the [snippet package from the AUR][aur].

Binaries for the releases can be downloaded from the [release page][releases].

When Go is installed on your machine, `go install gitlab.com/zj/snippet` will
install the binary to `$GOPATH/bin`. When added to your `$PATH`, it can be
invoked by executing `snippet`.

[aur]: https://aur.archlinux.org/packages/snippet/
[releases]: https://gitlab.com/zj/snippet/releases

### Usage

For authentication access to an access token is required. This token can be either
stored in `$HOME/.gitlab-token`, or passed with the `--token` flag.

```
NAME:
   snippet - Pipe to GitLab.com as Snippet

USAGE:
   snippet [global options] command [command options] [arguments...]

VERSION:
   0.4.0

COMMANDS:
     help, h  Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --file value, -f value        Create a snippet from file on disk instead of piped input
   --permission value, -p value  Set visibility of the snippet, one of: public, internal, or private (default: "public")
   --token value, -t value       Token if not using the $HOME/.gitlab-token file [$GITLAB_TOKEN]
   --host value                  Hostname of a GitLab istance to create the snippet on (default: "https://gitlab.com")
   --help, -h                    show help
   --version, -v                 print the version
```

[License](LICENSE)
